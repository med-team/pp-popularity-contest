Summary: PredictProtein popularity contest
Name: pp-popularity-contest
Version: 1.0.6
Release: 1
License: GPL
Group: Applications/Science
Source: ftp://rostlab.org/%{name}/%{name}-%{version}.tar.gz
URL: http://rostlab.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-root
BuildRequires: gcc-c++, boost-devel
Requires: crontabs, curl, perl, perl-URI

%define common_desc The pp-popularity-contest package sets up a cron job \
 that will periodically anonymously submit to the Rost Lab developers \
 statistics about the most used Rost Lab packages on this system. \
 . \
 This information helps us making decisions such as which packages \
 should receive high priority when fixing bugs. \
 It also helps us decide which packages should receive funding for further \
 development and support. \
 This information is also very important when the Rost Lab applies for funding. \
 . \
 Without the funding we receive based on the usage statistics you volunteer \
 none of the packages on this image could be made available to you at no cost.

%description
 %{common_desc}

%prep
%setup -q

%build
%configure LDFLAGS=-static
make

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=${RPM_BUILD_ROOT} install

mkdir -p  %{buildroot}%{_sysconfdir}/cron.daily
echo -e "#!/bin/sh\nif [ -x %{_datadir}/%{name}/cron.daily ]; then %{_datadir}/%{name}/cron.daily; fi\n" > %{buildroot}%{_sysconfdir}/cron.daily/%{name}
chmod +x %{buildroot}%{_sysconfdir}/cron.daily/%{name}

mkdir -p %{buildroot}%{_var}/spool/pp-popcon
chmod 1777 %{buildroot}%{_var}/spool/pp-popcon

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc AUTHORS
%doc COPYING
%{_bindir}/pp_popcon_cnt
%{_mandir}/*/*
%config %{_sysconfdir}/cron.daily/%{name}
%{_datadir}/%{name}/cron.daily
%dir %attr(1777,root,root) %{_var}/spool/pp-popcon

%changelog
* Thu Dec 1 2011 Laszlo Kajan <lkajan@rostlab.org> - 1.0.5-1
- new upstream release
* Mon Oct 31 2011 Laszlo Kajan <lkajan@rostlab.org> - 1.0.4-1
- new upstream release
* Wed Sep 28 2011 Laszlo Kajan <lkajan@rostlab.org> - 1.0.3-1
- new upstream release
* Wed Aug 31 2011 Laszlo Kajan <lkajan@rostlab.org> - 1.0.2-1
- static build now
* Tue Jun 21 2011 Laszlo Kajan <lkajan@rostlab.org> - 1.0.1-2
- spec in dist root
* Thu Jun 16 2011 Laszlo Kajan <lkajan@rostlab.org> - 1.0.1-1
- First rpm package
