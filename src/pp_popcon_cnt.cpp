#include <boost/program_options.hpp>
#include <boost/regex.hpp>
#include <errno.h>
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>

#include "config.h"

namespace cls = boost::program_options::command_line_style;
namespace po = boost::program_options;
namespace bo = boost;
using namespace std;

void              _print_help( const po::options_description& __opts );
namespace rostlab {
class PP_PopCon_Cnt
{
  private:
    bool          _dbg;
  public:
                  PP_PopCon_Cnt( bool __dbg = false ) : _dbg(__dbg) {}
    virtual       ~PP_PopCon_Cnt(){}

  void            record_count( const string& __program, const size_t __count )
  {
    // print __count '.'s into PPPOPCONSPOOL_DIR/__program
    string spoolfilename = string(PPPOPCONSPOOL_DIR) + '/' + __program;
    if(_dbg) cerr << "spool file name is '" << spoolfilename << "'\n";

    umask( 0111 );
    ofstream spoolfile( spoolfilename.c_str(), ios_base::out|ios_base::app );

    for( size_t i = 0; i < __count; ++i ) spoolfile << '.';
  }

  static bool     is_valid_program_name( const string& __name );
};
}
using namespace rostlab;

int main(int argc, char *argv[])
{
  if( setenv( "PATH", "/usr/local/bin:/usr/bin:/bin", 1) ) { cerr << strerror( errno ) << "\n"; return 1; }
  if( unsetenv("IFS") || unsetenv("CDPATH") || unsetenv("ENV") || unsetenv("BASH_ENV") ) { cerr << strerror( errno ) << "\n"; return 1; }

	//Use boost::program_options to store configuration
	po::options_description cmd_args("Command line arguments");
	cmd_args.add_options()
    ("debug", "debugging messages\n")
    ("help", "produce this help message\n")
		("program,p", po::value<string>(),"program to record usage for\n")
    ("count,c", po::value<int>()->default_value(1),"usage count to record, default: 1\n")
    ("version", "print version\n")
    ;

  //variables map, where variables and corresponding values are stored	
	po::variables_map vm;	

	//get command-line params and store them in vm
	po::store( po::parse_command_line(argc, argv, cmd_args, cls::default_style & ~cls::allow_guessing ), vm );

	if (vm.count("help")) { _print_help( cmd_args ); return 0; }
	if (vm.count("version")) { cout << PACKAGE_VERSION << "\n"; return 0; }
	
  po::notify(vm);    

  bool dbg = false; if( vm.count("debug") ) dbg = true;

  if( !vm.count("program") ) { cerr << "error: required argument --program is missing\n"; _print_help( cmd_args ); return 1; }
  if( !PP_PopCon_Cnt::is_valid_program_name( vm["program"].as<string>() ) ){ cerr << "error: invalid program name '" << vm["program"].as<string>() << "'\n"; return 1; }

  if( vm["count"].as<int>() < 1 ) { cerr << "error: --count can not be less than 1\n"; return 1; }

  PP_PopCon_Cnt pp_popcon_cnt( dbg );

  pp_popcon_cnt.record_count( vm["program"].as<string>(), vm["count"].as<int>() );

  return 0;
}


bool              PP_PopCon_Cnt::is_valid_program_name( const string& __name )
{
  // lkajan: try to do this without a regexp, or use <regex> from the STL
  //if( bo::regex_search( vm["program"].as<string>(), bo::regex("[^[:alnum:]._-]") ) ) { cerr << "error: invalid program name '" << vm["program"].as<string>() << "'\n"; return 1; }
  for( string::const_iterator s_i = __name.begin(); s_i != __name.end(); ++s_i )
  {
    if( !isalnum(*s_i) &&
        *s_i != '.' &&
        *s_i != '_' &&
        *s_i != '-'
    ) return false;
  }
  return true;
}


void              _print_help( const po::options_description& __opts )
{
  cout << __opts << "\n";
}


/*

=pod

=head1 NAME

pp_popcon_cnt - PredictProtein popularity contest usage counter

=head1 SYNOPSIS

pp_popcon_cnt [OPTION]

=head1 DESCRIPTION

The pp-popularity-contest package sets up a cron job
that will periodically anonymously submit to the Rost Lab developers
statistics about the most used Rost Lab packages on this system.

This information helps us making decisions such as which packages
should receive high priority when fixing bugs.
It also helps us decide which packages should receive funding for further
development and support.
This information is also very important when the Rost Lab applies for funding.

Without the funding we receive based on the usage statistics you volunteer
none of the packages on this image could be made available to you at no cost.

This binary collects the number of times a method was used.

=head1 OPTIONS

=over

=item --debug

=item --help

=item -p, --program

program to record usage for

=item -c, --count

usage count to record, default: I<1>

=item --version

=back

=head1 EXAMPLES

 pp_popcon_cnt --program preditprotein --count 1

=head1 FILES

=over

=item F<__PPPOPCONSPD__>

=back

=head1 AUTHOR

Laszlo Kajan

=head1 SEE ALSO

predictprotein(1), ppvmi(7)

=cut

*/

// vim:et:ts=2:ai:
